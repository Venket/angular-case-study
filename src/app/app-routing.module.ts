import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthguardServiceService } from './authguard-service.service';
import { EmployeeComponent } from './employee/employee.component';
import { HomeComponent } from './home/home.component';
import { LoginPageComponent } from './login-page/login-page.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent ,canActivate:[AuthguardServiceService] },
  { path: 'employee', component: EmployeeComponent,canActivate:[AuthguardServiceService] },
  { path: 'login', component: LoginPageComponent },
  { path: '',pathMatch: 'full', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
