import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthenticateServiceService } from './authenticate-service.service';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { EmployeeComponent } from './employee/employee.component';
import { HomeComponent } from './home/home.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { HeaderComponent } from './header/header.component';
import { EmployeeServiceService } from './employee/employee-service.service';
import { HeaderInterceptor } from './interceptor';
@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent,
    HomeComponent,
    LoginPageComponent,
    HeaderComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,HttpClientModule,FormsModule,ReactiveFormsModule
  ],
  providers: [AuthenticateServiceService,EmployeeServiceService,
    {provide: HTTP_INTERCEPTORS, useClass: HeaderInterceptor, multi: true} ],
  bootstrap: [AppComponent]
})
export class AppModule { }
