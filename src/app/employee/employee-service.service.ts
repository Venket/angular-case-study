import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeServiceService {


  constructor(private httpClient: HttpClient) { }
  employeeList: any = [];
  getEmployeeList() : Observable<any>{
    return this.httpClient.get("assets/employee.json");
  }
}
