import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Employee } from './employee';
import { EmployeeServiceService } from './employee-service.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(public emplService: EmployeeServiceService,private formBuilder: FormBuilder) {
    this.userForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['',[Validators.required]],
      email: ['', [Validators.required]]
    });
  }
  public employeeList: any= [] ;
  public headerList:any;
  employeeForm = new FormGroup({
    empid: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(20),
      Validators.pattern('^[a-zA-Z]+$'),
    ],),
    jobtitle: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(20),
      Validators.pattern('^[a-zA-Z]+$'),
    ]),
    name: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(20),
      Validators.pattern('^[a-zA-Z]+$'),
    ]),
  });

  model :any;

  submitted = false;
  isedit:boolean=false;
  isadd:boolean=false;
  onSubmit() {
let employee : any= {}
if(this.isadd){
  employee['empid']=this.employeeForm.controls['empid'].value
  employee['jobtitle']=this.employeeForm.controls['jobtitle'].value
  employee['name']=this.employeeForm.controls['name'].value
  this.employeeList.push(employee);
  this.employeeForm.reset();

}
else{
 let index=this.employeeList.findIndex((emp:Employee) => emp.empid ==this.employeeForm.controls['empid'].value);
 this.employeeList[index]['empid']=this.employeeForm.controls['empid'].value
 this.employeeList[index]['jobtitle']=this.employeeForm.controls['jobtitle'].value
 this.employeeList[index]['name']=this.employeeForm.controls['name'].value
 this.employeeForm.reset();

}
this.employeeForm.controls['empid'].enable()

  }

  cancel(){
    this.isadd=false;
    this.isedit=false;
  }

public userForm :FormGroup;
  ngOnInit(): void {
    this.emplService.getEmployeeList().subscribe(data =>{
      console.log(data);
      this.employeeList = data['Employees'];
      console.log(this.employeeList);
      this.headerList = Object.keys( this.employeeList[0]);
      console.log(this.headerList);


    })
  }
  editEmployee(row:Employee){

    this.employeeForm.patchValue({
      empid:row.empid,
      jobtitle:row.jobtitle,
      name:row.name

    })

    this.employeeForm.controls['empid'].disable()
    this.isedit=true;
  }

  addemployees(){
    this.employeeForm.patchValue({
      empid:'',
      jobtitle:'',
      name:''

    })
this.isadd=true;
  }
}
