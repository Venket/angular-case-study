export class Employee {

  constructor(
    public empid: string,
    public jobtitle: string,
    public name: string
  ) {  }

}
